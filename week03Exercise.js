// study regular expressions!

// IN-CLASS EXERCISE: EMAIL VALIDATION
const regex = /(\S+)\@(\S+\.\S+)/;

// IN-CLASS EXERCISE: BATTLE GAME

let fullHealth = 10;

// participant objects
const player1 = {
	health: fullHealth,
	name: 'Tat'
}

const player2 = {
	health: fullHealth,
	name: 'Monster'
}

// calculates total damage caused and subtracts health for defending player
function attack(attackingPlayer, defendingPlayer, baseDamage, variableDamage) {
	let maxVariableDamage = Math.ceil(Math.random() * variableDamage);
	let totalDamage = baseDamage + maxVariableDamage;
	defendingPlayer.health -= totalDamage;

	return `${attackingPlayer.name} hits ${defendingPlayer.name} for ${totalDamage} damage.`;

	console.log(attack(player1, player2, baseDamage, variableDamage));
	console.log(player2.health);
}

let dude = 'dude';
const car = {
	year: 2011,
	make: 'Subaru',
	message: function() {
		console.log(`${this.make} is your car, ${dude}!`)
	}
}

car.message();

// ITEMIZED RECEIPT
let list = {
	descr:[],
	price:[]
}

function logReceipt(item, cost) {
	let totalPrice = 0;
	let itemList = list.descr.push(item);
	let priceList = list.price.push(cost);
	for (let i = 0; i <= itemList - 1; i++) {
		console.log(`${list.descr[i]} - ${'$'}${list.price[i]}`);
		totalPrice += list.price[i];
	}
	console.log(`${'Total - '}${'$'}${totalPrice}`)
}

// NEW SPACESHIP
let SpaceShip = function SpaceShip(name, topSpeed) {
	const shipName = name;
	let shipTopSpeed = topSpeed;
	this.changeShipTopSpeed = function(newTopSpeed) {
		shipTopSpeed = newTopSpeed;
		return (`${shipName}'s top speed changed to ${newTopSpeed} mph!`)
	}
	this.acceleration = function() {
		return (`${shipName} moving to ${shipTopSpeed} mph!`);
	};
};

const rocket = new SpaceShip('Tat',50);
const shuttle = new SpaceShip('Atlantis',10000);